# Dbgpt Prompter

## Introduction

This script demonstrates the usage of the `Api` class from the `api` module to interact with a DB_GPT server. It initializes an instance of the `Api` class and starts a conversation either with an existing UID or by creating a new conversation.

### Prerequisites

- Before running the script, ensure you have the necessary modules installed:

```bash
git clone https://gitlab.com/kzm2/dbgpt-prompter.git

cd dbgpt-prompter

python3 -m venv .venv

source .venv/bin/activate # for mac or linux

pip install -r requirements.txt

```

### Instantiating the dbgpt_server
- create `.env` file 
```
API_KEY=""
API_BASE="http://www.exemple.com"
CHAT_MODE="chat_with_db_execute"
MODEL_NAME="chatgpt_proxyllm"
DB_NAME=""

```

- Save this code in a file named `server.py`

```python
# server.py

from api import Api as dbgpt_server

def main():
    # Instantiate the dbgpt_server
    inst = dbgpt_server()

    # Start a Conversation with Existing UID
    # inst.start_conversation("27af063e-bada-11ee-a0bc-0242ac120002")

    # Alternatively, to Start a New Conversation:
    inst.start_new_conversation()

if __name__ == "__main__":
    main()
```

### run :

```bash
python3 server.py
```
