import json, os
from bs4 import BeautifulSoup
from dbgpt_config import Config
from dbgpt_base import Base as dbgpt_Base
from colorama import Fore, Style
from dbgpt_conversation import dbgpt_serve_Conversation
from dotenv import load_dotenv


class Api(dbgpt_Base):
    """
    Represents an API interface

    Attributes:
        None

    Methods:
        __init__(self) -> None:
            Initializes an instance of the Api class.

        start_conversation(self, conv_uid: str) -> None:
            Starts a conversation using an existing conversation UID.

        start_new_conversation(self) -> None:
            Starts a new conversation and continuously prompts for user input.

        completions(self, prompt_input: str) -> None:
            Sends user input to the Db_GPT and prints the response.

        generate_response(self, resp: bytes) -> str:
            Generates a human-readable response from the raw server response.

    Usage:
    ```python
        # Example instantiation:
        api_instance = Api()

        # Example starting a conversation with an existing UID:
        api_instance.start_conversation(conv_uid="some_unique_id")

        # Example starting a new conversation:
        api_instance.start_new_conversation()
    """

    def __init__(self) -> None:
        """
            Initializes an instance of the Api class.
            Loads environment variables and sets up the dbgpt_serve_Conversation instance.
        """
        load_dotenv()
        super().__init__()
        self.dbgpt_conversation = dbgpt_serve_Conversation()

    def start_conversation(self, conv_uid):
        """
            Starts a conversation using an existing conversation UID.

            Args:
                conv_uid (str): Unique identifier of the existing conversation.
        """
        self.dbgpt_conv = self.dbgpt_conversation.check_Conv(conv_uid)
        while True:
            prompt_input = str(input(Fore.BLUE + "prompt: " + Style.RESET_ALL).strip())
            self.completions(prompt_input)

    def start_new_conversation(self):
        """
            Starts a new conversation and continuously prompts for user input.
        """
        self.dbgpt_conv = self.dbgpt_conversation.Create_Conv(
            chat_mode=self.chat_mode, user_name="kzm_Test_User", sys_code="", user_id=""
        )
        while True:
            prompt_input = str(input(Fore.BLUE + "prompt: " + Style.RESET_ALL).strip())
            self.completions(prompt_input)

    def completions(self, prompt_input):
        """
            Sends user input to the DB_GPT server and prints the response.

            Args:
                prompt_input (str): User's input prompt.
        """
        headers = {
            "accept": "application/json",
            "Content-Type": "application/json",
        }
        payload = {
            "conv_uid": self.dbgpt_conv["conv_uid"],
            "user_input": prompt_input,
            "user_name": "",
            "chat_mode": self.chat_mode,
            "select_param": self.select_param,
            "model_name": self.model_name,
            "incremental": False,
            "sys_code": "",
        }
        response = self._post("/api/v1/chat/completions", headers=headers, json=payload)
        if response.status_code == 200:
            # print(response.content)
            # Parse the HTML content
            soup = BeautifulSoup(response.content, "html.parser")

            # Find the chart-view element
            chart_view_element = soup.find("chart-view")

            if chart_view_element:
                # Extract the content attribute
                json_content = chart_view_element.get("content")

                # Decoding HTML entities and converting to JSON
                decoded_json_content = json.loads(json_content.replace("&quot;", '"'))

                # Now you have the JSON content
                print(
                    f"{Fore.RED}dbgpt :{Style.RESET_ALL}",
                    json.dumps(decoded_json_content, indent=2),
                )
            else:
                print(
                    f"{Fore.RED}dbgpt :{Style.RESET_ALL}",
                    self.generate_response(response.content),
                )
        else:
            print(f"Request failed with status code {response.status_code}. Response:")
            print(response.text)

    def generate_response(self, resp):
        """
            Generates a human-readable response from the raw server response.

            Args:
                resp (bytes): Raw server response.

            Returns:
                str: Human-readable response.
        """
        decoded_string = resp.decode("utf-8")
        # Extract the desired text
        start_index = decoded_string.find(":") + 2  # Add 2 to skip the space after ':'
        end_index = len(decoded_string)

        return decoded_string[start_index:end_index].strip()



