"""
    "chat_mode": "chat_with_db_execute",
    "select_param": "DB_GPT_TEST",
    "model_name": "chatgpt_proxyllm",
"""
import requests, os
from dbgpt_config import Config


class Base:
    """
    Base class providing common functionalities for interacting with an API.

    Attributes:
        chat_mode (str): The mode of the chat (e.g., "chat_with_db_execute", ...).
        select_param (str): The selected parameter or database name.
        model_name (str): The name of the model used in the API (e.g., "chatgpt_proxyllm", ...).
        incremental (bool): Flag indicating whether the interaction is incremental.
        sys_code (str): System code associated with the interaction.
        api_base (str): Base URL for the API.

    Methods:
        __init__(self, config: Config = None) -> None:
            Initializes an instance of the Base class.

        get_chat_mode(self) -> str:
            Returns the chat mode.

        get_select_param(self) -> str:
            Returns the selected parameter or database name.

        get_model_name(self) -> str:
            Returns the name of the model used in the API.

        get_incremental(self) -> bool:
            Returns the incremental flag.

        get_sys_code(self) -> str:
            Returns the system code associated with the interaction.

        _get(self, url, params=None, **kwargs) -> object:
            Performs a GET request to the API.

        _post(self, url, data=None, json=None, **kwargs) -> object:
            Performs a POST request to the API.
    """

    chat_mode: str = None
    select_param: str = None
    model_name: str = None
    incremental = False
    sys_code: str = None

    def __init__(self, config: Config = None) -> None:
        """
        Initializes an instance of the Base class.

        Args:
            config (Config): Configuration object containing settings for the API.
        """
        CONF = Config() if not config else config
        self.chat_mode = CONF.CHAT_MODE
        self.select_param = CONF.DB_NAME
        self.model_name = CONF.MODEL_NAME
        self.api_base = os.getenv("API_BASE")

    def get_chat_mode(self):
        return self.chat_mode

    def get_select_param(self):
        return self.select_param

    def get_model_name(self):
        return self.model_name

    def get_incremental(self):
        return self.incremental

    def get_sys_code(self):
        return self.sys_code

    def _get(self, url, params=None, **kwargs) -> object:
        """
        Performs a GET request to the API.

        Args:
            url (str): The endpoint URL.
            params (Optional[dict]): The parameters to include in the request.
            kwargs: Additional keyword arguments for the request.

        Returns:
            object: The response object.
        """
        request_URI = str(self.api_base) + str(url)
        return requests.get(request_URI, params=params, **kwargs)

    def _post(self, url, data=None, json=None, **kwargs) -> object:
        """
        Performs a POST request to the API.

        Args:
            url (str): The endpoint URL.
            data (Optional[dict]): The data to include in the request body.
            json (Optional[dict]): The JSON data to include in the request body.
            kwargs: Additional keyword arguments for the request.

        Returns:
            object: The response object.
        """
        request_URI = str(self.api_base) + str(url)
        return requests.post(request_URI, data=data, json=json, **kwargs)
