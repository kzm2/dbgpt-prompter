import os


class Config:
    """
        Configuration class for storing settings related to the API.

        Attributes:
            CHAT_MODE (str): The mode of the chat (e.g., "chat_with_db_execute", ...).
            DB_NAME (str): The selected parameter or database name.
            MODEL_NAME (str): The name of the model used in the API.

        Methods:
            __init__(self, api_base=None, chat_mode=None, db_name=None, model_name=None) -> None:
                Initializes an instance of the Config class.

        Usage:
        ```python
            # Example instantiation:
            config_instance = Config(api_base="http://example.com", chat_mode="chat_with_db_execute", db_name="may_db", model_name="chatgpt_proxyllm")
    """

    CHAT_MODE: str
    DB_NAME: str
    MODEL_NAME: str

    def __init__(
        self, api_base=None, chat_mode=None, db_name=None, model_name=None
    ) -> None:
        """
        Initializes an instance of the Config class.

        Args:
            api_base (str): Base URL for the API.
            chat_mode (str): The mode of the chat (e.g., "chat_with_db_execute", ...).
            db_name (str): The selected parameter or database name.
            model_name (str): The name of the model used in the API.
        """
        self.CHAT_MODE = os.getenv("CHAT_MODE") if not chat_mode else chat_mode
        self.DB_NAME = os.getenv("DB_NAME") if not db_name else db_name
        self.MODEL_NAME =os.getenv("MODEL_NAME") if not model_name else model_name
        os.environ["API_BASE"] = str(api_base) if api_base else os.getenv("API_BASE")
