from dbgpt_base import Base as dbgpt_Base


class dbgpt_serve_Conversation(dbgpt_Base):

    """
    Represents a conversation management class for interacting with  an open-source, data-domain large model framework (DB_GPT).

    Attributes:
        headers (dict): HTTP headers used for API requests.

    Methods:
        __init__(self, _config=None) -> None:
            Initializes an instance of the class.

        Create_Conv(self, chat_mode, user_name, user_id, sys_code) -> dict:
            Creates a new conversation

        check_Conv(self, conv_uid: str) -> dict:
            Checks and retrieves details of a conversation based on the provided conversation UID.

    Usage:
       ```python

        # Example instantiation:
        conversation_instance = dbgpt_serve_Conversation()

        # Example creating a new conversation:
        new_conv_data = conversation_instance.Create_Conv(chat_mode="text", user_name="John", user_id="123", sys_code="XYZ")

        # Example checking an existing conversation:
        existing_conv_data = conversation_instance.check_Conv(conv_uid="some_unique_id")
       ```
    """

    headers = {
        "accept": "application/json",
    }

    def __init__(self, _config=None) -> None:
        """
        Initializes an instance of the dbgpt_serve_Conversation class.

        Args:
            _config (Optional): Additional configuration parameters (if any).
        """
        super().__init__(_config)

    def Create_Conv(
        self,
        chat_mode,
        user_name: str = None,
        user_id: str = None,
        sys_code: str = None,
    ) -> dict:
        """
        Creates a new conversation on the debugging proxy server.

        Args:
            chat_mode (str): The mode of the chat (e.g., "chat_with_db_execute").
            user_name (Optional[str]): The name of the user participating in the conversation.
            user_id (Optional[str]): The unique identifier of the user.
            sys_code (Optional[str]): System code associated with the conversation.

        Returns:
            dict: Information about the created conversation.

        Example:
           ```python

            # Example usage:
            conversation_data = Create_Conv(chat_mode="chat_with_db_execute", user_name="mohamed", user_id="123", sys_code="XYZ")
        """
        conversation = None
        params: dict[str, str] = dict(
            chat_mode=chat_mode, user_name=user_name, user_id=user_id, sys_code=sys_code
        )
        response = self._post(
            "/api/v1/chat/dialogue/new", headers=self.headers, params=params
        )
        if response.status_code == 200:
            conversation = response.json()
        return conversation["data"]

    def check_Conv(self, conv_uid: str) -> dict:
        """
        Checks and retrieves details of a conversation based on the provided conversation UID.

        Args:
            conv_uid (str): Unique identifier of the conversation.

        Returns:
            dict: Information about the conversation with the specified UID.
        """

        conv = None
        params = dict(page="1", page_size="100")
        response = self._get(
            "/api/v1/chat/dialogue/list", headers=self.headers, params=params
        )

        if response.status_code == 200:
            Conv_List = response.json()
            for _conv in Conv_List["data"]:
                if conv_uid.strip() == _conv["conv_uid"]:
                    conv = _conv
        return conv
